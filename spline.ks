# Use graphical install
graphical

# Keyboard layouts
keyboard --vckeymap=de-nodeadkeys --xlayouts='de (nodeadkeys)'

# System language
lang de_DE.UTF-8

# Firewall configuration
firewall --use-system-defaults

# Run the Setup Agent on first boot
firstboot --enable

# Only consider disks that are likely to be the main disk. All but the first matching one will be ignored.
# * On NVME devices, the usb disk will be called sda, so stop after finding an nvme drive
# * In VMs, the usb disk will also be called sda, so stop after finding the first virtual drive
# * On SATA devices, sda is usually the internal drive, and the usb disk will be called sdb, so stop after finding sda.
ignoredisk --only-use=nvme0n1|vda|sda

# Partition clearing information
# clear all partitions on the nvme disk or if it does not exist use sda, 
# anaconda stops for confirmation
# note: for full automation add zerombr command to kickstart file
clearpart --all --drives=nvme0n1|vda|sda --initlabel

# Disk partitioning information
autopart --type btrfs

# System timezone
timezone Europe/Berlin --utc

# Lock root account by default (only use sudo)
rootpw --lock

# Set installation source
url --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-37&arch=x86_64"
repo --name=fedora --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-37&arch=x86_64"
repo --name=fedora-updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f37&arch=x86_64" --cost=0

%packages
@Fedora Workstation
@^workstation-product-environment
vim-enhanced
ghc
nasm
gcc
gcc-c++
clang
autoconf
automake
git
htop
tmux
okular
ranger
pandoc
python3-numpy
python3-scipy
python3-matplotlib
inkscape
krita
foliate
libreoffice
xournalpp
geary
gnome-tweaks
gnome-firmware
gnome-extensions-app
gnome-shell-extension-places-menu
gnome-shell-extension-appindicator
gnome-shell-extension-gsconnect
@development-tools
flatpak
initial-setup
NetworkManager-openconnect 
%end

%post
  # Add flathub
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

  # Add RPM Fusion
  dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

  dnf -y groupupdate core
  dnf -y groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
  dnf -y groupupdate sound-and-video
  dnf -y install rpmfusion-free-release-tainted
  dnf -y install rpmfusion-nonfree-release-tainted

  # Multimedia
  dnf -y install x264 vlc ffmpeg

  dnf -y install gstreamer1-plugins-{good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel

  # Flatpaks
  flatpak install -y flathub com.github.tchx84.Flatseal org.pipewire.Helvum

  # remove abrt
  dnf -y remove gnome-abrt totem

  # Install vscode
  rpm --import https://packages.microsoft.com/keys/microsoft.asc
  sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
  dnf check-update
  dnf -y install code
%end
