# Installparty USB disk image builder

## Development setup

1. Run `./create-ventoy.sh` to build the image.
1. In Virt-Manager, create a new virtual machine with the presets of a current version of Fedora.
1. Edit the virtual machine, and add a new storage device.
	1. Choose a custom image (the ventoy.img we just built)
	1. Choose bus type USB
1. Edit the bootorder to prefer the new USB drive.
1. Start the virtual machine. It should now complain about secure boot, this is unfortunately realistic, and will also happen on real hardware.
1. Enroll ventoys secure boot key using the menu that the UEFI should show.
1. Start the installer
